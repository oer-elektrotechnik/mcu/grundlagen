# Ein- und Ausgabepins in Variablen speichern

Bei dem vorherigen Beispiel ist es an Hand des Programmcodes sehr schwer nachzuvollziehen, was die Schaltung macht, weil im Programmkontext nicht ersichtlich ist, was zum Beispiel am Pin `D8` angeschlossen ist. Folglich muss man das Gesamtporgramm kennen, um zu erfahren, was bei `digitalWrite(D8, HIGH);` passiert.

Wenn später die Schaltung angepasst werden muss, und z.B. die rote LED an `D5` angeschlossen werden muss, muss das gesamte Programm angepasst werden.

Um diese beiden Probleme zu beheben, wollen wir die Anschlusspins in Konstanten speichern.

```arduino
/*Liste der Ein- und Ausgabelemente und Deklaration der Pins:
Datentyp | Name       | Anschlusspin   | Name, Eigenschaften*/
const int LED_ROT      = D8;            // highactive
const int LED_GELB     = D7;            // highactive
const int LED_GRUEN    = D6;            // highactive

const int kurzePause = 300;
const int langePause = 3000;

void setup() {
  pinMode(LED_ROT, OUTPUT);
  pinMode(LED_GELB, OUTPUT);
  pinMode(LED_GRUEN, OUTPUT);
}

void loop() {
  //grün
  digitalWrite(LED_GRUEN, HIGH);
  digitalWrite(LED_GELB, LOW);
  digitalWrite(LED_ROT, LOW);
  delay(langePause);

  //gelb
  digitalWrite(LED_GRUEN, LOW);
  digitalWrite(LED_GELB, HIGH);
  delay(kurzePause);

  //rot
  digitalWrite(LED_GELB, LOW);
  digitalWrite(LED_ROT, HIGH);
  delay(langePause);

  // rot-gelb
  digitalWrite(LED_GELB, HIGH);
  digitalWrite(LED_ROT, HIGH);
  delay(kurzePause);
}
```


## Weitere Literatur und Quellen





## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/](https://gitlab.com/oer-informatik/).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
