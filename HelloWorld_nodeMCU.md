# Microcontroller "Hello World" - Blinken!!!

Für ein erstes "Hello World" nutzen wir die intern verbauten LED und Taster des nodeMCU: Eine LED auf dem Microcontrollerboard, die mit dem Ausgang `D4` verschaltet ist, eine LED auf dem nodeMCU-Board, die mit dem Ausgang `D0` verschaltet ist und ein Taster ("FLASH"), der am Eingang D3 angeschlossen ist:

![Die Lage der internen LED und Taster auf dem nodeMCU.-Board](images/Foto_interneLEDTaster.jpg)

Wir nutzen die Arduino-IDE, um diese internen LED und Taster anzusteuern. Ein Arduino-Sketch besteht im wesentlichen aus fünf Komponenten, von denen drei durch uns konfigurierbar sind:

- Nach dem Start wird eine Funktion `init()` aufgerufen, die alle nötigen Bibliotheken läd und konfiguriert. Diese Methode wird uns nicht angezeigt und ist nicht konfigurierbar.

- Danach kommt ein Bereich, in dem wir eigene Variablen erzeugen können, die wir in unserem Sketch benötigen. Das werden wir später nutzen.

- Darauf folgt eine `setup`-Methode, in der wir alle Konfigurationen vornehmen können, die für unser Programm erforderlich sind. Die `setup()`-Methode wird genau einmal aufgerufen.

- Nach der `setup()`-Methode folgt eine Dauerschleife. Die Schleife selbst sehen wir nicht, wir wissen nur, dass in ihr eine weitere Methode aufgerufen wird:

- Die `loop()`-Methode befindet sich in der Dauerschleife. In der `loop()`-Methode befindet sich die eigentliche Programmlogik - entweder direkt, oder in Form von Funktionsaufrufen.

Grafisch lässt sich das ganze als Programm-Ablaufplan (PAP) darstellen:^[Grafik wurde erstellt mit dem PAP-Designer von Friedrich Folkmann: [http://friedrich-folkmann.de/papdesigner/Hauptseite.html](http://friedrich-folkmann.de/papdesigner/Hauptseite.html)]

![Die Lage der internen LED und Taster auf dem nodeMCU.-Board](images/PAP_des_Hauptprogramms.png)
## LED am Prozessorchip ESP-12

Eine interne LED ist am ESP-12 über 470 Ohm mit dem GPIO `D4` verbunden. Wir können sie mit ein paar Zeilen Code ein- und ausschalten.

![Lage der internen ESP-12-LED](images/Foto_interneLED_ESP.jpg)

Das Programm besteht aus wenigen Zeilen Code:

In der `setup()`-Methode werden alle Operationen aufgerufen, die genau ein einziges Mal beim Starten ausgeführt werden müssen. Hier wird konfiguriert, dass es sich bei dem an `D4` angeschlossenen Gerät um ein Ausgabedevice handelt:

```arduino
void setup() {
  pinMode(D4, OUTPUT);    // define Pin D4 as an Output
}
```

In einer zweiten Methode, die in Arduino-Sketches immer vorkommt, werden alle Operationen aufgerufen, die in einer Dauerschleife laufen sollen. Hier wird an den Ausgabepin `D4` abwechselnd ein `HIGH`- (3,3V) und ein `LOW`-(0V) Potenzial angelegt. Die LED sollte also blinken.

```arduino
void loop() {
  digitalWrite(D4, HIGH);   // set HIGH potential on D4 (3,3V)
  delay(1000);              // wait for a second (1000ms)
  digitalWrite(D4, LOW);    // set LOW potential on D4 (0V)
  delay(100);               // wait for 0,1 seconds (100ms)
}
```

Das Potezial wird lange auf `HIGH` und kurz auf `LOW` gelegt - wir erwarten also ein langes leuchten und eine kurze Pause. Wie blinkt die LED aber wirlich?

Es ist genau umgekehrt: lange Pause, kurzes Leuchten! Um das Verhalten zu verstehen müssen wir uns die interne Schaltung ansehen:

![Schaltplan der LED des ESP8266-Chips](images/nodeMCU_interneLED_ESP.png)

Sobald an `D4` ein `LOW`-Signal anliegt ergibt sich eine Spannungs-Potenzialdifferenz (3,3V - 0V), die zu einem Stromfluss durch die LED führt. Liegt an `D4` ein `HIGH`-Potenzial an, so ergibt sich keine Potenzialdifferenz (beide Seiten auf 3,3V) und die LED leuchtet nicht. Die LED verhält sich also gerade umgekehrt, als man es erwarten würde: sie ist bei einem anliegenden `LOW`-Potenzial aktiv, man nennt dies auch _low-active_.

## LED am nodeMCU-Board (nahe des Micro-USB-Ports)

Es gibt eine zweite LED am nodeMCU-Board, zwischen dem "RST"-Taster und den gelben Bauelementen:

![Lage der internen NodeMCU-LED](images/Foto_interneLED_nodeMCU.jpg)

Diese LED ist genauso an `D0` verschaltet wie die vorige LED an `D4`:

![Die interne LED des nodeMCU-Boards ist über 470 Ohm an D0 verschaltet](images/nodeMCU_interneLED_node.png)

Wir können daher alle Überlegungen von oben übernehmen, auch das Programm sieht genauso aus, wir müssen lediglich `D4` durch `D0` ersetzen:

```arduino
void setup() {
  pinMode(D0, OUTPUT);    // define Pin D0 as an Output
}

void loop() {
  digitalWrite(D0, HIGH);   // set HIGH potential on D0 (3,3V) => LED off (3V3-3V3, no current)
  delay(1000);              // wait for a second (1000ms)
  digitalWrite(D0, LOW);    // set LOW potential on D0 (0V) => LED on (GND-3V3, current)
  delay(100);               // wait for 0,1 seconds (100ms)
}
```

## Taster am nodeMCU-Board

Der `FLASH`-Taster, der neben dem Micro-USB-Port sitzt, lässt sich ebenfalls direkt für Programme nutzen. Er darf nur beim Booten des ESP-Boards nicht betätigt sein, danach kann er frei genutzt werden:

![Schaltplan der LED des ESP8266-Chips](images/Foto_internerTaster.jpg)

Intern ist er mit dem `D3`-Eingang verschaltet und verfügt über einen _Pull-Up_-Widerstand. Das heißt: wenn der Taster nicht betätigt ist, liegt ein `HIGH`-Signal an `D3` an.

![Die interne LED des nodeMCU-Boards ist über 470 Ohm an D0 verschaltet](images/nodeMCU_internerTaster.png)

Bei Betätigung liegt (näherungsweise) ein `LOW`-Signal an. Die Potenzialdifferenz zwischen `GND` (0V) und `3V3` (3,3V) teilt sich im Verhältnis der Widerstände auf:

$$
U_{D3} = 3,3 V \cdot \frac{430 \Omega}{12.000 \Omega + 430 \Omega} = 0,11 V
$$

Gemäß Datenblatt erkennt der ESP8266 Eingangssignale zwischen $-0,3V$ und $0,8 V$ gesichert als `LOW`, wir sind somit also auf der sicheren Seite. Der Arduino-Sketch ist folgendermaßen aufgebaut:

- In der `setup()`-Methode wird der Pin `D3` als Eingang festgelegt: `pinMode(D3, INPUT);`. Die LED an Pin `D4` wird wieder als Ausgang genutzt.

- In der Dauerschleife (`loop()`) wird ausgelesen, ob das anliegende Potenzial an `D3` ein `LOW`-Pegel ist: `if (digitalRead(D3)==LOW){...}`.
- Bei anliegendem `LOW`-Pegel wird die LED an  `D4` aktiviert (wie im obigen Beispiel).
- Für alle anderen Fälle (also bei anliegendem `HIGH`-Pegel) wird im `else{...}`-Block die LED deaktiviert.

Im Ganzen sieht das so aus:

```arduino
void setup() {
  pinMode(D3, INPUT);
  pinMode(D4, OUTPUT);
}

void loop() {
  if (digitalRead(D3)==LOW){
        digitalWrite(D4, LOW);    // set LOW potential on D4 (0V) => LED on (GND-3V3, current)
    }else{
        digitalWrite(D4, HIGH);   // set HIGH potential on D4 (3,3V) => LED off (3V3-3V3, no current)
    }
}
```

## Wo befinden sich die versteckten Programmbestandteile?

Für alle, die es genau wissen wollen (alle anderen können diesen Absatz getrost überspringen):

Eingangs wurde erwähnt, dass es über die beiden oben genannten Funktionen `setup()` und `loop()` hinaus noch weiteren Programmcode gibt, den die Arduino-IDE aber vor uns versteckt. Gibt es eine Möglichkeit danach zu forschen? Gibt es beispielsweise eine `main()`-Methode, wie sie die _Java_ und _C/C++_ Programmierer kennen?

Ja, die gibt es, man kann sie tief im Arduino-Ordner aufspüren, unter Windows beispielsweise:

```bash
C:\
  Program Files\
   arduino-x.x.x\
     hardware\
       arduino\
         avr\
           cores\
             arduino\
               main.cpp
```

In dieser `main.cpp`-Datei befindet sich die `main()`-Methode des C++-Programms. Und darin befinden sich die eingangs erwähnten Methodenaufrufe (siehe Programmablaufplan oben) `init()`, `setup()` und `loop()`, sowie die Dauerschleife (hier über `for (;;) {`...}` umgesetzt):

```cpp
int main(void)
{
	init();

	initVariant();

#if defined(USBCON)
	USBDevice.attach();
#endif

	setup();

	for (;;) {
		loop();
		if (serialEventRun) serialEventRun();
	}

	return 0;
}

```

## Weitere Literatur und Quellen

-



## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/](https://gitlab.com/oer-informatik/).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
