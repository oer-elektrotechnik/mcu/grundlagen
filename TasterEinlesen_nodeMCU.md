# Externe Taster am NodeMCU-Board einlesen

Die Pins des NodeMCU haben als GPIO (_general purpose input output_) die Möglichkeit, ein `HIGH`-Signal oder ein `LOW`-Signal an einem Eingang zu erkennen. Es können daher Taster angeschlossen werden, und darüber `HIGH` oder `LOW` erzeugt werden.

## Beispiele ohne Schalter - und Gefahren für den USB-Port


## Variante 1a: Verschaltung gegen `HIGH`

## Variante 1b: Verschaltung gegen `LOW`

## Variante 2: Statt Schließer (NO, _normally opened_) einen Öffner (NC, _normally closed_)





```arduino
/*Liste der Ein- und Ausgabelemente und Deklaration der Pins:
Datentyp | Name       | Anschlusspin   | Name, Eigenschaften*/
const int LED_ROT      = D8;            // highaktiv
const int LED_GELB     = D7;            // highaktiv
const int LED_GRUEN    = D6;            // highaktiv

const int TASTER_ROT   = 5;             // Schließer mit Arduino-internen Pulldown-Widerstand
const int TASTER_GRUEN = 4;             // Schließer mit Arduino-internen Pulldown-Widerstand
const int TASTER_BLAU  = 3;             // Schließer mit Arduino-internen Pulldown-Widerstand
const int TASTER_GELB  = 2;             // Schließer mit Arduino-internen Pulldown-Widerstand

const int kurzePause = 300;
const int langePause = 3000;

void setup() {
  pinMode(LED_ROT, OUTPUT);
  pinMode(LED_GELB, OUTPUT);
  pinMode(LED_GRUEN, OUTPUT);
}

void loop() {
  //grün
  digitalWrite(LED_GRUEN, HIGH);
  digitalWrite(LED_GELB, LOW);
  digitalWrite(LED_ROT, LOW);
  delay(langePause);

  //gelb
  digitalWrite(LED_GRUEN, LOW);
  digitalWrite(LED_GELB, HIGH);
  delay(kurzePause);

  //rot
  digitalWrite(LED_GELB, LOW);
  digitalWrite(LED_ROT, HIGH);
  delay(langePause);

  // rot-gelb
  digitalWrite(LED_GELB, HIGH);
  digitalWrite(LED_ROT, HIGH);
  delay(kurzePause);
}
```

## Variante 3: Tasterstatus merken

```arduino

  // Variante 1: Leuchtet nur bei Betätigung
  if (digitalRead(TASTER_ROT)){
      digitalWrite(LED_ROT, HIGH);
    } else {
      digitalWrite(LED_ROT, LOW;
    } 
  // Variante 2: Leuchtet bis zur nächsten Betätitung

   if (digitalRead(TASTER_ROT)==LOW){
    led_an = led_an * (-1);
    delay(500);
    }
   
   if (led_an == 1){
      digitalWrite(LED_ROT, HIGH);
    } else {internem 
      digitalWrite(LED_ROT, LOW);
    } 
```
    
## Weitere Literatur und Quellen





## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/](https://gitlab.com/oer-informatik/).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
