# Grundlagen Microcontroller-Programmierung

* ["Hello World" des Microcontrollers nodeMCU](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/HelloWorld_nodeMCU.html) [(als PDF)](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/HelloWorld_nodeMCU.pdf)
* [Externe LED mit dem nodeMCU ansteuern](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/externeLEDansteuern_nodeMCU.html) [(als PDF)](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/externeLEDansteuern_nodeMCU.pdf)
* [Taster einlesen](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/TasterEinlesen_nodeMCU.html) [(als PDF)](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/TasterEinlesen_nodeMCU.pdf)
* [Variablen und Datentypen](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/VariablenUndDatentypen_nodeMCU.html) [(als PDF)](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/VariablenUndDatentypen_nodeMCU.pdf)
* [Lauflicht mit Array und Schleifen](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/LauflichtMitArrayUndSchleifen_nodeMCU.html) [(als PDF)](https://oer-elektrotechnik.gitlab.io/mcu/grundlagen/LauflichtMitArrayUndSchleifen_nodeMCU.pdf)

# Infos zur genutzten gitlab CI und deren Nachnutzung:
Die HTML und PDF-Dateien wurden mit Hilfe der Konfiguraiton der [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab erstellt.
Die Vorlage findet sich hier: [https://gitlab.com/TIBHannover/oer/course-metadata-test/](https://gitlab.com/TIBHannover/oer/course-metadata-test/).
