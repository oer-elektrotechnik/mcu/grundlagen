# Externe LEDs am Arduino anschließen

Wenn wir externe LED mit dem NodeMCU ansteuern wollen, müssen wir uns zunächst mit den elektronischen Spezifikationen der LED vertraut machen. Unser NodeMCU-Board liefert an den einzelnen Pins `HIGH` = 3,3V oder `LOW`= 0V, somit können wir LED an einer Spannung von 3,3V betreiben. Typische Betriebsspannungen der LED sind etwa:

|LED Farbe|Betriebsspannung|
|---|---|
|rot| 1,6 - 2,2 V|
|gelb| 1,9 - 2,5V|
|grün| 1,9 - 2,5V|
|blau|2,7 - 3,5 V|
|weiß|2,7 - 3,5 V|

## Einfacher Fall: blaue und weiße LED

Eine blaue oder weiße LED können wir also an 3,3V direkt anschließen, bei den anderen Farben müssen wir uns noch etwas überlegen (dazu später mehr).

Wir haben zwei Möglichkeiten, die benötigten 3,3V zu erhalten:

* zwischen einem Pin (z.B. `D6`) und GND (0V), wenn wir `D6` auf `HIGH` (also 3,3V) stellen (high-active)

![Schaltplan: LED highactive direkt an 3,3V zwischen GND und D6](images/LED_an_3V3_highactive.png)

* zwischen einem Pin (z.B. `D6`) und 3V3 (3,3V), wenn wir `D6` auf `LOW` (also 0V) stellen (low-active)

![Schaltplan: LED lowactive direkt zwischen 3V3 und D6](images/LED_an_3V3_lowactive.png)

Wichtig hierbei ist, dass wir die LED mit dem Pluspol an den jeweilig positiveren Pin anschließen. Der Pluspol (die Anode) ist daran zu erkennen, dass er von allem mehr hat: mehr Kragen und mehr (längeres) Beinchen. Der Minuspol der LED (Kathode) ist an der LED abgeflacht und hat ein kürzeres Beinchen. Ist die LED verkehrt herum verbunden, sperrt sie und leuchtet nicht.

![Schaltplan: LED highactive direkt an 3,3V zwischen GND und D6](images/LED-Aufbau.PNG)

## LED mit Vorwiderstand

Im Fall einer roten, gelben oder grünen LED müssen wir die anliegende Spannung auf unter 3,3V mindern, um die LED nicht zu zerstören. Wir fügen ein zusätzliches Bauteil in den Stromkreis ein: einen Vorwiderstand. Und wir nutzen eine der Grundgesetzmäßigkeiten des elektrischen Stromkreises, das zweite Kirchhoff'sche Gesetz:

> Die Summe aller Spannungen beim Umlauf in einer geschlossenen Masche ist Null.

![LED mit Vorwiderstand](images/maschenregel.png)

Wir haben in unserem Stromkreis die Spannungsquelle (`D6` und `GND`) mit $U_{ges} =3,3V$, die LED (an einer roten LED sollen $U_{LED}=1,6V$ fallen) und einen noch unbekannten Vorwiderstand $R_{V}$, an dem die Spannung $U_{V}$ fällt.

Es gilt also:

$$\sum{U_i}=0 $$
Die Spannungen müssen in einem Maschendurchlauf (Pfeilrichtung) summiert werden, wenn der Pfeil gegen die Umlaufrichtung verläuft (bei $U_{ges}$) mit negativem Vorzeichen!
$$\Rightarrow  U_{V} + U_{LED} - U_{ges} = 0$$
$$\Rightarrow  U_{V}  = U_{ges} - U_{LED} = 3,3V - 1,6 V = 1,7 V$$

Am Vorwiderstand müssen also 1,7V fallen, damit die LED am Betriebspunkt betrieben werden kann.

Um den Widerstand dimensionieren zu können, müssen wir dem Datenblatt der LED noch die Betriebsstromstärke entnehmen. Üblicherweise liegt diese bei LED um die $I_{LED} = 20 mA$. Da in unverzweigten Stromkreisen die Stromstärke an jedem Punkt identisch ist, können wir diese Stromstärke auch für den Vorwiderstand annehmen.

Mit Hilfe der Gleichung für den elektrischen Widerstand können wir den benötigten Vorwiderstandswert ausrechnen:

$$ R_{V} = \frac{U_{V}}{I_{LED}} = \frac{1,7V}{0,20A} = 85 \frac{V}{A} = 85 \Omega $$

Da nicht alle Widerstandsgrößen als Bauteil vorhanden sind und ein zu hoch gewählter Widerstand nur zu einer etwas geringeren Leuchtkraft der LED führt, wird in der Regel der nächst höhere Widerstandswert gewählt.

Widerstände können als Bauteil gekauft werden und sind in Serien sortiert, die unterschiedlich viele Werte je Dekade (je Faktor 10) haben. Die _E3_ Reihe hat z.B. drei Werte je Dekade: $10 \Omega, 22 \Omega, 47 \Omega$ und dann in der nächsten Dekade: $100 \Omega, 220 \Omega, 470 \Omega$ usw. Bei der _E6_ Reihe sind es entsprechend sechs Werte je Dekade, bei der E12-Reihe zwölf und so weiter.

![Widerstandswerte - Übersicht der E-Reihen E3 - E24](images/Widerstandsreihen.png)


Oft liegen nur die Widerstände der E3-Reihe zwischen $100 \Omega$ und $10 k\Omega$ vor. Bei nodeMCU-Controllern kann dann für alle LED-Farben zum $100 \Omega$-Widerstand gegriffen werden (bei blauen und weißen LED kann ggf. auf diesen verzichtet werden - siehe Datenblatt oder eigene Messung). Bei 5V-basierten Microcontrollern wie dem Arduino wäre für alle LED ein $220 \Omega$-Widerstand passend. Wenn präzisere E-Reihen vorliegen, können die Widerstände an Hand der folgenden Tabelle ausgewählt werden:

|LED Farbe|Betriebsspannung|errechneter Vorwiderstandswert an 3,3V Microcontroller (z.B: nodeMCU), ausgewählte Widerstände je E-Reihe|errechneter Vorwiderstandswert an 5 V Microcontroller (z.B: Arduino), ausgewählte Widerstände je E-Reihe|
|---|---|---|---|
|rot| 1,6 - 2,2 V| $55 \Omega - 85 \Omega$<br>E3/E6/E12: $100 \Omega$<br>E24: $91 \Omega$|$140 \Omega - 170 \Omega$ <br>E3/E6: $220 \Omega$<br>E12/E24: $180 \Omega$|
|gelb| 1,9 - 2,5V| $40 \Omega - 70 \Omega$<br>E3/E6: $100 \Omega$<br>E12/E24: $82 \Omega$|$125 \Omega - 155 \Omega$ <br>E3/E6: $220 \Omega$<br>E12/E24: $180 \Omega$|
|grün| 1,9 - 2,5V| $40 \Omega - 70 \Omega$<br>E3/E6: $100 \Omega$<br>E12/E24: $82 \Omega$|$125 \Omega - 155 \Omega$ <br>E3/E6: $220 \Omega$<br>E12/E24: $180 \Omega$|
|blau|2,7 - 3,5 V| $0 \Omega - 30 \Omega$<br>E3: $47 \Omega$<br>E6/E12/E24: $33 \Omega$|$75 \Omega - 125 \Omega$ <br>E3: $220 \Omega$<br>E6/E12/E24: $150 \Omega$|
|weiß|2,7 - 3,5 V| $0 \Omega - 30 \Omega$<br>E3: $47 \Omega$<br>E6/E12/E24: $33 \Omega$|$75 \Omega - 125 \Omega$ <br>E3: $220 \Omega$<br>E6/E12/E24: $150 \Omega$|

Da die Widerstände als Bauteil zu klein sind, um Werte darauf aufzudrucken, wurde eine Farbcodierung entwickelt. Vier oder fünf Farbringe beschreiben den jeweiligen Wert:

- Bei vier Ringen: zwei Zahlenwerte, ein Multiplikator, ein Ring zur Angabe der Toleranz (silber oder gold).

- Bei fünf Ringen: drei Zahlenwerte, ein Multiplikator, ein Ring zur Angabe der Toleranz (grün, blau, violett).

![Farbcodierung von Widerstandswerten](images/Widerstandswerte.png)


Ein $100 \Omega (5\%)$ Widerstand mit vier Ringen hat die Farbfolge: braun-schwarz-braun-gold. Weitere verbreitete Beispiele mit 4 Ringen:

![Farbcodierung von Widerstandswerten](images/Widerstandsringe-Beispiel-4Ring.png)

Ein $100 \Omega (1\%) $ Widerstand mit fünf Ringen hat die Farbfolge: braun-schwarz-schwarz-schwarz-braun. Weitere verbreitete Beispiele mit 5 Ringen:

![Farbcodierung von Widerstandswerten](images/Widerstandsringe-Beispiel-5Ring.png)


## Aufbau der Schaltung mit externen LED auf einem Breadboard

Das Breadboard führt oben und unten je eine blaue Schiene für Ground (`GND`) und eine rote Schiene für 3,3 Volt (`3V3`), die waagerecht durchkontaktiert sind und mit den entsprechenden Anschlüssen des NodeMCU verbunden werden sollten.

In der Mitte des Breadboards sind oberhalb und unterhalb der mittigen Nut jeweils 5 Pins senkrecht durchkontaktiert.

Wir schließen die LED an, in dem wir von der unteren `GND`-Schiene kommend auf eine Spalte  der unteren Hälfte gehen, von dort über einen $100 \Omega$ - Widerstand auf die obere Hälfte, dort in der selben Spalte die Kathode der LED anschließen (das kürzere Beinchen). Die Annode der LED steckt in der benachbarten Spalte, von wo eine Brücke zu einem der Digitalen Ausgänge `D6`, `D7` und `D8` geht.

![Aufbau einer Ampel mit dem NodeMCU und den Anschlüssen D6, D7 und D8](images/AmpelschaltungFritzing.png)

EIn zugehöriger Sketch nutzt die selben Elemente, die wir schon beim "Hello World" kennengelernt hatten - nur mit den Anschlüssen `D6`, `D7` und `D8`. Diesmal sind die LED _highactive_, leuchten also bei anliegendem `HIGH`-Signal. Eine Ampelschaltung könnte etwa so aussehen:


```arduino
void setup() {
  pinMode(D6, OUTPUT);    // define Pin D6 as an Output
  pinMode(D7, OUTPUT);    // define Pin D7 as an Output
  pinMode(D8, OUTPUT);    // define Pin D8 as an Output
}

void loop() {
  //grün
  digitalWrite(D6, HIGH);
  digitalWrite(D7, LOW);
  digitalWrite(D8, LOW);
  delay(3000);              // wait for three seconds (3000ms)

  //gelb
  digitalWrite(D6, LOW);
  digitalWrite(D7, HIGH);
  delay(1000);              // wait for a second (1000ms)

  //rot
  digitalWrite(D7, LOW);
  digitalWrite(D8, HIGH);
  delay(3000);              // wait for three seconds (3000ms)

  // rot-gelb
  digitalWrite(D7, HIGH);
  digitalWrite(D8, HIGH);
  delay(1000);              // wait for a second (1000ms)
}
```

## Weitere Literatur und Quellen

-



## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/](https://gitlab.com/oer-informatik/).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
